﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public class GlobalStats : MonoBehaviour {

	public static int score, strikes, HighScore;
	public static float moveTimer = 6, movetimerN, difficultyscaler;
	public float TimerChecker;

	public Text ScoreTxt, HighScoreTxt, EndGameHS;
	public RawImage Strike1,Strike2,Strike3;
	public Texture StrikeX, StrikeN;

	public GameObject EndGame, StartGame;
	public GameObject parentGO;
	public static bool IsPlaying;

	// Use this for initialization
	void Awake () {
		Load ();
		IsPlaying = false;

	}
	
	// Update is called once per frame
	void Update () {
		moveTimer -= Time.deltaTime;
		movetimerN = 6.00f - (difficultyscaler);
		TimerChecker = moveTimer;
		if (IsPlaying) {
			Time.timeScale = 1;
			StartGame.SetActive(false);
		} else {
			Time.timeScale = 0;
			StartGame.SetActive(true);
		}

		// difficulty scaler, change to change how difficulty scales.
		if (score >= 2 && score <= 5) {
			difficultyscaler = 1.5f;
		} else if (score >= 6 && score <= 10) {
			difficultyscaler = 2;
		} else if (score >= 11 && score <= 15) {
			difficultyscaler = 2.5f;
		} else if (score >= 16 && score <= 20) {
			difficultyscaler = 3;
		} else if (score >= 21 && score <= 30) {
			difficultyscaler = 3.5f;
		} else if (score >= 31 && score <= 50) {
			difficultyscaler = 4;
		} else if (score >= 51 && score <= 80) {
			difficultyscaler = 5;
		} else if (score >= 81) {
			difficultyscaler = 5.5f;
		} else {
			difficultyscaler = 1;
		}
		// Checks strikes.
		if (strikes <= 0) {
			Strike1.texture = StrikeN;
			Strike2.texture = StrikeN;
			Strike3.texture = StrikeN;
		} else if (strikes == 1) {
			Strike1.texture = StrikeX;
			Strike2.texture = StrikeN;
			Strike3.texture = StrikeN;
		} else if (strikes == 2) {
			Strike1.texture = StrikeX;
			Strike2.texture = StrikeX;
			Strike3.texture = StrikeN;
		} else if (strikes >= 3) {
			Strike1.texture = StrikeX;
			Strike2.texture = StrikeX;
			Strike3.texture = StrikeX;
			GameOver();
		}
		ScoreTxt.text = "Score = " + score.ToString ();
		HighScoreTxt.text = "High Score = " + HighScore.ToString ();
		EndGameHS.text =  "High Score = " + HighScore.ToString ();
	}
	public void GameOver(){
		IsPlaying = false;
		EndGame.SetActive (true);
		if (score >= HighScore) {
			HighScore = score;
			Save (); 
			DeleteAllChildren();
		}
	}
	public void PlayAgain(){
		DeleteAllChildren();
		IsPlaying = true;
		EndGame.SetActive (false);
		score = 0;
		strikes = 0;
		SwipeControls.SelectedObjectNum = 1;
		ObjectSelecter.LastObjectNum = 0;
	}
	public void PlayGame()
	{
		IsPlaying = true;
		StartGame.SetActive (false);
	}
	
	void DeleteAllChildren()
	{
		foreach (Transform child in parentGO.transform) {
			Destroy (child.gameObject);
		}
	}
	public static void Save() {
		BinaryFormatter bf = new BinaryFormatter();
		//Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
		FileStream file = File.Create (Application.persistentDataPath + "/savedata.st"); //you can call it anything you want
		PlayerData data = new PlayerData();
		data.HS = HighScore;

		
		bf.Serialize(file, data);
		file.Close();
		Debug.Log ("Game is Saved");
		print (Application.persistentDataPath);
	}
	public static void Load() {
		if(File.Exists(Application.persistentDataPath + "/savedata.st")) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/savedata.st", FileMode.Open);
			PlayerData data = (PlayerData)bf.Deserialize(file);
			file.Close();
			HighScore = data.HS;
			
			Debug.Log ("Game is loaded");
		}
	}
	public static void Delete () {
		//Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
		System.IO.File.Delete (Application.persistentDataPath + "/savedata.st"); //you can call it anything you want
		Debug.Log ("Game is Deleted");
	}
	public void OnApplicationFocus (bool focused)
	{
		if (focused){
			Load ();
		}
		if (!focused) {
			Save ();
			IsPlaying = false;
			
		}
	}
	public void Exit(){
		Save ();
		Application.Quit ();
	}

}
[Serializable]
class PlayerData
{
	public int HS;
	
}
