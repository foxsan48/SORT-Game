﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class SwipeControls : MonoBehaviour,IBeginDragHandler,IDragHandler, IEndDragHandler {

	public static GameObject CurrentSelectedObject,  NextSelectedObject, ThirdSelectedObject;
	public static Vector2 OnePosition;
	public static bool IsDragging;

	public static Vector2 oldPosition1;
	
	public static GameObject ItemBeingDragged1;
	
	public static int SelectedObjectNum;

	public Vector2 StartDragPos;
	public Vector2 EndDragPos, CurrentDragPos, NewItemDragPos1, DragPosDiff1, BeginMousePos;


	public bool X;
	public bool Y;
	public bool Right;
	public bool Left;
	public bool Down;
	public bool Up;

	

	public int TextureNum;

	public AudioSource Correct, Wrong, BGM, Swipe;

	void Start() {
		SelectedObjectNum = 1;
		
	}

	void Update() {
		Debug.Log (CurrentSelectedObject + " is CurrentSelectedObject");
		Debug.Log ("Selected Object Number is " + SelectedObjectNum);

	}


	

	#region IBeginDragHandler implementation
	public void OnBeginDrag (PointerEventData eventData)
	{
		X = false;
		Y = false;
		//ItemBeingDragged1 = CurrentSelectedObject;
		oldPosition1 = CurrentSelectedObject.transform.position;
		StartDragPos = CurrentSelectedObject.transform.position;
		BeginMousePos = Input.mousePosition;
		IsDragging = true;

		
	}
	#endregion
	
	#region IDragHandler implementation
	
	public void OnDrag (PointerEventData eventData)
	{
		CurrentDragPos = Input.mousePosition;
		DragPosDiff1 = BeginMousePos - CurrentDragPos;
		NewItemDragPos1 = oldPosition1 - DragPosDiff1 ;
		if (X == false && Y == false) {
			if (CurrentDragPos.x > (BeginMousePos.x + 70)) {
				X = true;
			} else if (CurrentDragPos.x < (BeginMousePos.x - 70)) {
				X = true;
			} else if (CurrentDragPos.y > (BeginMousePos.y + 70)) {
				Y = true;
			} else if (CurrentDragPos.y < (BeginMousePos.y - 70)) {
				Y = true;
			}
		}
		if (X == true) { 
			CurrentSelectedObject.transform.position = new Vector2 (NewItemDragPos1.x, oldPosition1.y);
			print ("X is true");
		} else if (Y == true) {
			CurrentSelectedObject.transform.position = new Vector2 (oldPosition1.x, NewItemDragPos1.y);
			print ("Y is true");
		}
	}
	
	#endregion
	
	#region IEndDragHandler implementation
	
	public void OnEndDrag (PointerEventData eventData)
	{
		EndDragPos = CurrentSelectedObject.transform.position;
		CurrentSelectedObject.transform.position = oldPosition1;
		IsDragging = false;
			CheckSwipe ();
	
			X = false;
			Y = false;
	}
	#endregion
	void CheckSwipe(){
		if (EndDragPos.x > (StartDragPos.x + 120)) {
			Debug.Log ("Right");
			Right = true;
			Swipe.Play();
			Green ();
		} else if (EndDragPos.x < (StartDragPos.x - 120)) {
			Debug.Log ("Left");
			Left = true;
			Swipe.Play();
			Blue ();
		}
		if (EndDragPos.y > (StartDragPos.y + 150)) {
			Debug.Log ("Up");
			Up = true;
			Swipe.Play();
			Red ();
		} else if (EndDragPos.y < (StartDragPos.y - 150)) {
			Debug.Log ("Down");
			Down = true;
			Swipe.Play();
			Yellow ();
		}
		ActivateSwipe ();
	}
	void ActivateSwipe() {
		if (X == true && Y == true) {
			X = false;
			Y = false;
			Debug.Log ("Conflict");

		} else if (X == true && Y != true && Right == true) {
			Debug.Log ("Green");
			X = false;
			Y = false;
		} else if (X == true && Y != true && Left == true) {
			Debug.Log ("Blue");
			X = false;
			Y = false;
		} else if (Y == true && X != true && Up == true) {
			Debug.Log ("Red");
			X = false;
			Y = false;
		} else  if (Y == true && X != true && Down == true) {
			Debug.Log ("Yellow");
			X = false;
			Y = false;
		}
	}
	 void Green (){
		if (CurrentSelectedObject.GetComponent<ObjectStats>().Class == 1) 
		{
			GlobalStats.score += 1;
			Destroy (SwipeControls.CurrentSelectedObject);
			SelectedObjectNum += 1;
			CurrentSelectedObject = NextSelectedObject;
			NextSelectedObject = null;
			Correct.Play();

		} else if (CurrentSelectedObject.GetComponent<ObjectStats>().Class != 1) {
			GlobalStats.strikes += 1;
			Destroy (SwipeControls.CurrentSelectedObject);
			SelectedObjectNum += 1;
			CurrentSelectedObject = NextSelectedObject;
			NextSelectedObject = null;
			Wrong.Play();
		}
	}
	 void Blue(){
		if (CurrentSelectedObject.GetComponent<ObjectStats>().Class == 2) 
		{
			GlobalStats.score += 1;
			Destroy (SwipeControls.CurrentSelectedObject);
			SelectedObjectNum += 1;
			CurrentSelectedObject = NextSelectedObject;
			NextSelectedObject = null;
			Correct.Play();

		} else if (CurrentSelectedObject.GetComponent<ObjectStats>().Class != 2) {
			GlobalStats.strikes += 1;
			Destroy (SwipeControls.CurrentSelectedObject);
			SelectedObjectNum += 1;
			CurrentSelectedObject = NextSelectedObject;
			NextSelectedObject = null;
			Wrong.Play();

		}
	}
	void Yellow(){
		if (CurrentSelectedObject.GetComponent<ObjectStats>().Class == 3) 
		{
			GlobalStats.score += 1;
			Destroy (SwipeControls.CurrentSelectedObject);
			SelectedObjectNum += 1;
			CurrentSelectedObject = NextSelectedObject;
			NextSelectedObject = null;
			Correct.Play();

		} else if (CurrentSelectedObject.GetComponent<ObjectStats>().Class != 3) {
			GlobalStats.strikes += 1;
			Destroy (SwipeControls.CurrentSelectedObject);
			SelectedObjectNum += 1;
			CurrentSelectedObject = NextSelectedObject;
			NextSelectedObject = null;
			Wrong.Play();
		}
	}
	void Red(){
		if (CurrentSelectedObject.GetComponent<ObjectStats>().Class == 4) 
		{
			GlobalStats.score += 1;
			Destroy (SwipeControls.CurrentSelectedObject);
			SelectedObjectNum += 1;
			CurrentSelectedObject = NextSelectedObject;
			NextSelectedObject = null;
			Correct.Play();

		} else if (CurrentSelectedObject.GetComponent<ObjectStats>().Class != 4) {
			GlobalStats.strikes += 1;
			Destroy (SwipeControls.CurrentSelectedObject);
			SelectedObjectNum += 1;
			CurrentSelectedObject = NextSelectedObject;
			NextSelectedObject = null;
			Wrong.Play();
		}
	}
}