﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObjectStats : MonoBehaviour {

	public int item, Class, Num;
	public int AssClass, AssObject, AssNum;
	public static GameObject ThisObject;
	public bool IsStartup;
	public Vector3 StartLoc, EndLoc;
	public float speed = 1.50f, LifeSpanCheck;
	public float LifeSpan = 18, NewLifeSpan = 23;

	public Texture Green, Blue, Red, Yellow, Newspaper, Callender, MilkC, Books, Box, TissueBox, CoffeeCup, JuiceBox,
		TissuePaper, PizzaBox;


	

	// Use this for initialization
	void Start () {
		if (IsStartup) {
			LifeSpan = 18;
		} else {
			if (Class == 1) {
				if (item == 1) {
					gameObject.GetComponent<RawImage> ().texture = Green;
				} else if (item == 2) {
					gameObject.GetComponent<RawImage> ().texture = Green;
				} else if (item == 3) {
					gameObject.GetComponent<RawImage> ().texture = Green;
				} else if (item == 4) {
					gameObject.GetComponent<RawImage> ().texture = Green;
				} else if (item == 5) {
					gameObject.GetComponent<RawImage> ().texture = Green;
				} else if (item == 6) {
					gameObject.GetComponent<RawImage> ().texture = Green;
				} else if (item == 7) {
					gameObject.GetComponent<RawImage> ().texture = Green;
				} else if (item == 8) {
					gameObject.GetComponent<RawImage> ().texture = Green;
				} else if (item == 9) {
					gameObject.GetComponent<RawImage> ().texture = Green;
				} else if (item == 10) {
					gameObject.GetComponent<RawImage> ().texture = Green;
				}
			} else if (Class == 2) {
				if (item == 1) {
					gameObject.GetComponent<RawImage> ().texture = Blue;
				} else if (item == 2) {
					gameObject.GetComponent<RawImage> ().texture = Blue;
				} else if (item == 3) {
					gameObject.GetComponent<RawImage> ().texture = Blue;
				} else if (item == 4) {
					gameObject.GetComponent<RawImage> ().texture = Blue;
				} else if (item == 5) {
					gameObject.GetComponent<RawImage> ().texture = Blue;
				} else if (item == 6) {
					gameObject.GetComponent<RawImage> ().texture = Blue;
				} else if (item == 7) {
					gameObject.GetComponent<RawImage> ().texture = Blue;
				} else if (item == 8) {
					gameObject.GetComponent<RawImage> ().texture = Blue;
				} else if (item == 9) {
					gameObject.GetComponent<RawImage> ().texture = Blue;
				} else if (item == 10) {
					gameObject.GetComponent<RawImage> ().texture = Blue;
				}
			} else if (Class == 3) {
				if (item == 1) {
					gameObject.GetComponent<RawImage> ().texture = Newspaper;
				} else if (item == 2) {
					gameObject.GetComponent<RawImage> ().texture = TissueBox;
				} else if (item == 3) {
					gameObject.GetComponent<RawImage> ().texture = TissuePaper;
				} else if (item == 4) {
					gameObject.GetComponent<RawImage> ().texture = PizzaBox;
				} else if (item == 5) {
					gameObject.GetComponent<RawImage> ().texture = Box;
				} else if (item == 6) {
					gameObject.GetComponent<RawImage> ().texture = Books;
				} else if (item == 7) {
					gameObject.GetComponent<RawImage> ().texture = Callender;
				} else if (item == 8) {
					gameObject.GetComponent<RawImage> ().texture = CoffeeCup;
				} else if (item == 9) {
					gameObject.GetComponent<RawImage> ().texture = JuiceBox;
				} else if (item == 10) {
					gameObject.GetComponent<RawImage> ().texture = MilkC;
				}
			} else if (Class == 4) {
				if (item == 1) {
					gameObject.GetComponent<RawImage> ().texture = Red;
				} else if (item == 2) {
					gameObject.GetComponent<RawImage> ().texture = Red;
				} else if (item == 3) {
					gameObject.GetComponent<RawImage> ().texture = Red;
				} else if (item == 4) {
					gameObject.GetComponent<RawImage> ().texture = Red;
				} else if (item == 5) {
					gameObject.GetComponent<RawImage> ().texture = Red;
				} else if (item == 6) {
					gameObject.GetComponent<RawImage> ().texture = Red;
				} else if (item == 7) {
					gameObject.GetComponent<RawImage> ().texture = Red;
				} else if (item == 8) {
					gameObject.GetComponent<RawImage> ().texture = Red;
				} else if (item == 9) {
					gameObject.GetComponent<RawImage> ().texture = Red;
				} else if (item == 10) {
					gameObject.GetComponent<RawImage> ().texture = Red;
				}
			}
			Debug.Log ("New Object Num is " + Num);
		}
	}
	
	// Update is called once per frame
	 void Update () {
		LifeSpan -= Time.deltaTime;
		LifeSpanCheck = LifeSpan;
		Debug.Log ("New object num is " + Num);
		transform.Translate(Vector2.down * Time.deltaTime * speed, Camera.main.transform);
		if ( SwipeControls.CurrentSelectedObject == null){
			if (Num == SwipeControls.SelectedObjectNum) {
				SwipeControls.CurrentSelectedObject = gameObject;
			}
		}
		if (LifeSpan <= 0) {
			Destroy(gameObject);
			GlobalStats.strikes += 1;
			}
		if (GlobalStats.score >= 5 && GlobalStats.score <= 10) {
			speed = 50;
		} else if (GlobalStats.score >= 11 && GlobalStats.score <= 29) {
			speed = 100;
		} else if (GlobalStats.score >= 30) {
			speed = 150;
		} else {
			speed = 30;
		}
	}
//	void OnCollisionEnter(Collision collision) {
//		if (collision.gameObject.tag == "DeathPanel") {
//			Destroy(gameObject);
//			GlobalStats.strikes -= 1;
//		}
}