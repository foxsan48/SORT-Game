﻿using UnityEngine;
using System.Collections;

public class ObjectSelecter : MonoBehaviour {

	public static int LastObjectNum, NewObjectNum ,NewObjClass;
	public GameObject NewObj, Startpoint ;
	public GameObject Objects;
	public float DifficultySpiker = 15;


	// Use this for initialization
	void Start () {
		LastObjectNum = 1;
	}
	
	// Update is called once per frame
	void Update () {
		NewObjectNum = LastObjectNum + 1;
		if (GlobalStats.IsPlaying) {
			DifficultySpiker -= Time.deltaTime;
		} else {
		}

		if (DifficultySpiker <= 0) {
			StartCoroutine(Spike());
			DifficultySpiker = Random.Range(15,25);
		}

		if (GlobalStats.moveTimer <= 0) {
			CreateNewObject();
			GlobalStats.moveTimer = GlobalStats.movetimerN;
		}
	}

	 void CreateNewObject()
	{
		GameObject NewObject = Instantiate (NewObj) as GameObject;
		NewObject.transform.SetParent (Objects.transform);
		NewObject.transform.position = Startpoint.transform.position;
		NewObject.GetComponent<ObjectStats> ().Num = NewObjectNum;
		NewObject.GetComponent<ObjectStats> ().Class = Random.Range (1, 5);
		NewObject.GetComponent<ObjectStats> ().item = Random.Range (1, 11);
		// Create LifeSpan on creation
		if (GlobalStats.score >= 5 && GlobalStats.score <= 10) {

			NewObject.GetComponent<ObjectStats> ().LifeSpan = 18;
		} else if (GlobalStats.score >= 11 && GlobalStats.score <= 29) {
			NewObject.GetComponent<ObjectStats> ().LifeSpan = 8;
		} else if (GlobalStats.score >= 30) {
			NewObject.GetComponent<ObjectStats> ().LifeSpan = 5;
		} else {
			NewObject.GetComponent<ObjectStats> ().LifeSpan = 28;
		}
		LastObjectNum = NewObjectNum;

	}

	public IEnumerator Spike()
	{
		yield return new WaitForSeconds (1.5f);
		CreateNewObject ();
		yield return new WaitForSeconds (1.5f);
		CreateNewObject ();
		yield return new WaitForSeconds (1.5f);
		CreateNewObject ();
		print ("Spike");


	}
}
